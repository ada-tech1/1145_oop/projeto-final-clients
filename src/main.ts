import './style.css'
import Client from './model/Client'
import ClientRepository from './model/ClientRepositoryArray'
import ListaClienteView from './view/ClientListView'


class Main {
  
  private static clientRepository: ClientRepository = new ClientRepository()
  private static listClientsView: ListaClienteView = new ListaClienteView(this.clientRepository)

  static start() {
    
    Main.registerListeners()


  }

  private static registerListeners() {

    const addButton = document.querySelector('#add-client') as HTMLButtonElement
    addButton?.addEventListener('click', (event: Event) => {
      event.preventDefault()
      const clientName = document.querySelector('#input-name') as HTMLInputElement
      if (clientName) {
        const client = new Client(clientName.value)
        Main.clientRepository.add(client)
        Main.listClientsView.render()
        clientName.value = ""
        clientName.focus()
      }
    })


    const listButton = document.querySelector('#list-clients') as HTMLButtonElement
    listButton?.addEventListener('click', () => {
      Main.listClientsView.render()
    })

   
  }

}

Main.start()