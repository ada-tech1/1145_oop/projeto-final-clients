export const mlUUID = (): string => {
    let templateUUID = 'xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx' 
    let timestampForCalc = new Date().getTime()
    let performaceForCalc = typeof performance !== 'undefined' && performance.now && performance.now() * 1000 || 0

    let generatedUUID: string = templateUUID.replace(/[xMN]/g, (templateChar): string => {
        let mArray: string[] = ['1', '2', '3', '4', '5']
        let nArray: string[] = ['8', '9', 'a', 'b']
        let initialArray: string = 'abcdefghijlmnopqrstuvxz'
        let finalChar = initialArray.charAt(Math.floor(Math.random() * initialArray.length))
        if (templateChar === 'x') {

            let randomSalt = Math.random() * 16
            if (timestampForCalc > 0) {
                randomSalt = (timestampForCalc + randomSalt) % 16 | 0
                timestampForCalc = Math.floor(timestampForCalc/16)
            } else {
                randomSalt = (performaceForCalc + randomSalt) % 16 | 0
                performaceForCalc = Math.floor(performaceForCalc/16)
            }
            finalChar =  (randomSalt).toString(16)
        }

        if (templateChar === 'M') {
            finalChar =  mArray[Math.floor(Math.random() * mArray.length)]
        }

        if (templateChar === 'N') {
            finalChar = nArray[Math.floor(Math.random() * nArray.length)]
        }
        
        
        
        return finalChar

    })

    // console.log(`Someone is calling me. Here is the id I've calculated: ${generatedUUID}`)

    return generatedUUID
}