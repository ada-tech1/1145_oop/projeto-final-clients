import ClientI from "../interfaces/Client.I";
import ClientRepositoryI from "../interfaces/ClientRepository.I";


export default class ClientRepositoryArray implements ClientRepositoryI {
    
    private clientRepositoryArray: ClientI[] = []

    add(client: ClientI): void {
        const jaExiste = this.clientRepositoryArray.find(clientItem => clientItem.getId() === client.getId())
        if (!jaExiste) {
            this.clientRepositoryArray.push(client)
        }
        
    }

    remove(client: ClientI): void {
        let clientEncontrado = this.clientRepositoryArray.find(clientItem => clientItem.getId() === client.getId())
        if (clientEncontrado) {
            this.clientRepositoryArray.splice(this.clientRepositoryArray.indexOf(client), 1)
        }
    }

    list(): ClientI[] {
        return this.clientRepositoryArray
    }
    
}