import ClienteI, {ClienteId} from "../interfaces/Client.I"
import { mlUUID } from "../utils/ml-simple-uuid"



export default class Cliente implements ClienteI {
    
    private id: string
    
    constructor(private nome: string) {
        this.id = mlUUID()
    }

    toString() {
        return `Name: ${this.nome}`
    }

    getId(): ClienteId {
        return this.id
    }
}


