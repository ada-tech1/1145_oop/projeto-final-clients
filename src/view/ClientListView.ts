import ClientI from "../interfaces/Client.I";
import ClientRepositoryI from "../interfaces/ClientRepository.I";

export default class ClientListView {

    constructor(private clientRepository: ClientRepositoryI) {}

    private mountClientItem(client: ClientI): HTMLLIElement {
        const li = document.createElement('li')
        li.setAttribute('class', 'clients-list__item')
        const spanName = document.createElement('span')
        spanName.textContent = client.toString()

        li.append(spanName)
        li.append(this.setDelButton(client))
        
        return li
    }

    private setDelButton(client: ClientI): HTMLButtonElement {
        const delButton = document.createElement('button')
        delButton.setAttribute('class', 'clients-list__item--del')
        delButton.setAttribute('id-client', client.getId())
        delButton.textContent = 'delete'
        delButton.addEventListener('click', (event: Event) => {
            event.preventDefault()
            this.clientRepository.remove(client)
            this.render()
        })
        return delButton
    }

    render() {
        let ulTarget = document.querySelector('#clients-list')
        if (ulTarget) {
            ulTarget.innerHTML = ''
            for (const client of this.clientRepository.list()) {

                const li = this.mountClientItem(client)
                ulTarget.append(li)
                
            }
        }
    }
}