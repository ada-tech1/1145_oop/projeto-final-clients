import ClienteI from "./Client.I";

export default interface ListaClienteI {
    add(cliente: ClienteI): void
    remove(cliente: ClienteI): void
    list(): ClienteI[]
}