export type ClienteId = string

export default interface ClienteI {
    toString(): string
    getId(): ClienteId
}